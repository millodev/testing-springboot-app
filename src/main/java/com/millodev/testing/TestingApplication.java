/**
 * TestingApplication.java
 */

package com.millodev.testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class TestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestingApplication.class, args);
	}

	@Controller
	class DefaultPage {

		@RequestMapping(value = { "/", "welcome" })
		public String defaultPage() {
			return "defaultPage";
		}

	}

}